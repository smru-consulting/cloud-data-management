import subprocess
from pydub import AudioSegment
from pydub.utils import mediainfo
import xml.etree.ElementTree as ET
from datetime import datetime
import wave

javapath = "C:\\Program Files\\Java\\jdk-15.0.1\\bin\\java.exe"
jarlocation = "C:\\AzureDevelopment\\cloud-data-management\\PamBuoyDataManipulation\\RecordingUnpack\\Jar\\X3WavConvert.jar"
AudioSegment.ffmpeg = "C:\\Program Files\\FFMPEG\\bin\\ffmpeg.exe"
AudioSegment.converter = "C:\\Program Files\\FFMPEG\\bin\\ffmpeg.exe"

#Recycling Doug's x3 converter written in java. Refactored the coder to take source and destination input so it could run as a batch process
#Input: source: full source file path to a wav file including .x3 extension
#Input: destination: full file path to the desired output location of an wav file with .wav extension
def x3toWAV(source,destination):
    subprocess.call(['java', '-jar', jarlocation,'-source',source,"-destination",destination])

def getRecordingMeta(filePaths):
    recMeta = {'wav':{},'x3a':{},'mp3':{}}
    tree = ET.parse(filePaths['xml'])
    root = tree.getroot()
    for CFG in root.findall("CFG"):
        if CFG.find('PROC').text=="X3V2":
            fs = CFG.find('FS').text
            bd = CFG.find('NBITS').text
            nchans = CFG.find('NCHS').text
            recMeta["x3a"]["Fs"] = float(fs)
            recMeta["x3a"]["bitDepth"] = float(bd)
            recMeta['x3a']['nChans'] = int(nchans)
        if CFG.find('PROC').text=="WAV":
            fs = CFG.find('FS').text
            bd = CFG.find('NBITS').text
            nchans = CFG.find('NCHS').text
            recMeta["wav"]["Fs"] = float(fs)
            recMeta["wav"]["bitDepth"] = float(bd)
            recMeta['wav']['nChans'] = int(nchans)
    wav = wave.open(filePaths['wav'],'rb')
    recMeta['duration_seconds'] = wav.getnframes()/wav.getframerate()
    return recMeta

#Simple conversion from WAV to mp3.
#Input: source: full source file path to a wav file including .wav extension
#Input: destination: full file path to the desired output location of an mp3 file with .mp3 extension
#Returns: sample rate of single channel mp3 file
#TODO: make sure the actual sample rate of the mp3 is returned by this function. play around with output volume, channel manipulation, etc
def WAVtoMP3(source,destination):
    wav = AudioSegment.from_wav(source)
    wav.export(destination,format='mp3')
    return 44800
