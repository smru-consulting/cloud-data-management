from PamBuoyDataManipulation.BinaryUnpack.pamBinRead import pamBinRead

TIMEMILLIS = int('1',16)
TIMENANOS= int('2',16)  
CHANNELMAP = int('4',16) 
UID = int('8',16)   
STARTSAMPLE = int('10',16)   
SAMPLEDURATION = int('20',16)    
FREQUENCYLIMITS = int('40',16)     
MILLISDURATION  = int('80',16)  
TIMEDELAYSSECS  = int('100',16)   
HASBINARYANNOTATIONS = int('200',16)
HASSEQUENCEMAP  = int('400',16)   
HASNOISE  = int('800',16) 
HASSIGNAL  = int('1000',16)  
HASSIGNALEXCESS  = int('2000',16) 

def readPamData(file,fileInfo):
    data = {}
    data["flagBitMap"] = 0
    data["hasAnnotation"] = 0
    curObj = file.tell()
    objectLen = pamBinRead(file,'int32',n=1)
    nextObj = curObj+objectLen

    data["identifier"] = pamBinRead(file,'int32',n=1)

    isBackground = data["identifier"]==-6

    version = fileInfo["fileHeader"]["fileFormat"]

    data["millis"] = pamBinRead(file,'int64',n=1)


    if(version>=3):
        data["flagBitMap"] = pamBinRead(file,'int16',n=1)

    if((version==2) or (data["flagBitMap"]  & TIMENANOS)!=0):
        data["timeNanos"] = pamBinRead(file,'int64',n=1)
    
    if((version==2) or (data["flagBitMap"]  & CHANNELMAP)!=0):
        data["channelBitmap"] = pamBinRead(file,'int32',n=1)

    if(data["flagBitMap"] & UID) == UID:
        data["UID"] = pamBinRead(file,'int64',n=1)
    
    if(data["flagBitMap"] & STARTSAMPLE) != 0:
        data["startSample"] = pamBinRead(file,'int64',n=1)

    if(data["flagBitMap"] & SAMPLEDURATION) != 0:
        data["sampleDuration"] = pamBinRead(file,'int32',n=1)

    if(data["flagBitMap"] & FREQUENCYLIMITS) != 0:
        data["minFreq"] = pamBinRead(file,'float',n=1)
        data["maxFreq"] = pamBinRead(file,'float',n=1)
    
    if(data["flagBitMap"] & MILLISDURATION) != 0:
        data["millisDuration"] = pamBinRead(file,'float',n=1)

    if(data["flagBitMap"] & TIMEDELAYSSECS) != 0:
        data["numTimeDelays"] = pamBinRead(file,'int16',n=1)
        td = [0]*data["numTimeDelays"]
        for i in range(data['numTimeDelays']):
            td[i] = pamBinRead(file,'float',n=1)
        data["timeDelays"] = td

    if(data["flagBitMap"] & HASSEQUENCEMAP) != 0:
        data["sequenceMap"] = pamBinRead(file,'int32',n=1)

    if(data["flagBitMap"] & HASNOISE) != 0:
        data["noise"] = pamBinRead(file,'float',n=1)
    
    if(data["flagBitMap"] & HASSIGNAL) != 0:
        data["signal"] = pamBinRead(file,'float',n=1)
    
    if(data["flagBitMap"] & HASSIGNALEXCESS) != 0:
        data["signalExcess"] = pamBinRead(file,'float',n=1)
    
    data["date"] = data['millis']/1000


    if(isBackground):
        file.seek(nextObj,1)
    
    else:
        if(callable(fileInfo["readModuleData"])):
            data = fileInfo["readModuleData"](file,fileInfo,data,False)

    if(data["flagBitMap"] & HASSIGNALEXCESS) != 0:
        file.seek(nextObj,1)

    return data