from PamBuoyDataManipulation.BinaryUnpack.pamBinRead import pamBinRead
from PamBuoyDataManipulation.BinaryUnpack.readStdModuleHeader import readStdModuleHeader

def readNoiseMonHeader(fid):
    header = readStdModuleHeader(fid)
    if header['binaryLength']!=0:
        header['nBands'] = pamBinRead(fid,'int16',n=1)
        header['statsTypes'] = pamBinRead(fid,'int16',n=1)
        lowEdges = [0 for i in range(header['nBands'])]
        for i in range(header["nBands"]):
            lowEdges[i] = pamBinRead(fid,'float',n=1)
        highEdges= [0 for i in range(header['nBands'])]
        for i in range(header['nBands']):
            highEdges[i] = pamBinRead(fid,'float',n=1)

        header['bands'] = [(lowEdges[i],highEdges[i]) for i in range(header['nBands'])]

    return header