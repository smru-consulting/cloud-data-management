from PamBuoyDataManipulation.BinaryUnpack.pamBinRead import pamBinRead
import statistics

def readWMData(file,moduleHeader,data,skipLarge):
    
    #data={}
    dataLength = pamBinRead(file,'int32',n=1)
    startPos = file.tell()
    #Though Deicmus is version 2, and the readPamData logic works with version 2, 
    #The logic in the WhistleMoan unpacking works only if its version 1. Still haven't gotten to the bottom of this. 
    version = 1#moduleHeader['version']


    if int(version)<=1:
        data['startSample'] = pamBinRead(file,'int64',n=1)
        data["channelMap"] = pamBinRead(file,'int32',n=1)

    data['nSlices']=pamBinRead(file,'int16',n=1)

    if int(version)>=1:
        data['amplitude'] = pamBinRead(file,'int16',n=1)/100

    if int(version)<2:
        data["nDelays"] = pamBinRead(file,'int8',n=1)
        data["delays"] = []
        for i in range(data['nDelays']):
            data['delays'].append(pamBinRead(file,'float',n=1))

    if skipLarge:
        file.seek(startPos + dataLength,0)
        return data
        
    data["sliceData"] = []
    data["contour"] = [0 for i in range(data['nSlices'])]
    data["contWidth"] = [0 for i in range(data['nSlices'])]

    for i in range(data['nSlices']):
        aSlice = {}
        aSlice['sliceNumber'] = pamBinRead(file,'int32',n=1,seek=skipLarge)
        aSlice["nPeaks"] = pamBinRead(file,'int8',n=1)
        aSlice["peakData"] = [[0 for i in range(4)] for j in range(aSlice["nPeaks"])]
        for p in range(aSlice["nPeaks"]):
            for s in range(4):
                sss = pamBinRead(file,'int16',n=1,seek=skipLarge)
                aSlice["peakData"][p][s]=sss
        data["sliceData"].append(aSlice)
        data["contour"][i] = aSlice["peakData"][0][1]
        data["contWidth"][i] = aSlice["peakData"][0][2] - aSlice["peakData"][0][0] + 1
    
    if data["nSlices"]>0:
        data["meanWidth"] = statistics.mean(data["contWidth"])
    else:
        data["meanWidth"] = 0

    return data