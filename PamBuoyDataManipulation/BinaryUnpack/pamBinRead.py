from struct import unpack
def pamBinRead(fid,what,n,seek=False):
    

    if seek:
        if n==0:
            return
        if what=="int8":
            return fid.seek(n,1)
        elif what=="uint8":
            return fid.seek(n,1)
        elif what=="int16":
            return fid.seek(n*2,1)
        elif what=="uint16":
            return fid.seek(n*2,1)
        elif what=="int32":
            return fid.seek(n*4,1)
        elif what=="int64":
            return fid.seek(n*8,1)
        elif what=="float":
            return fid.seek(n*4,1)
        elif what=="double":
            return fid.seek(n*8,1)
        elif what=="character":
            return fid.seek(n,1)
        else:
            return

    byte=-1
    if n==0:
        return
    if what=="int8":
        byte=fid.read(1)
        return int.from_bytes(byte,byteorder='big',signed=True)
    elif what=="uint8":
        byte=fid.read(1)
        return int.from_bytes(byte,byteorder='big',signed=False)
    elif what=="int16":
        byte=fid.read(2)
        return int.from_bytes(byte,byteorder='big',signed=True)
    elif what=="uint16":
        byte=fid.read(2)
        return int.from_bytes(byte,byteorder='big',signed=False)
    elif what=="int32":
        byte=fid.read(4)
        return int.from_bytes(byte,byteorder='big',signed=True)
    elif what=="int64":
        byte=fid.read(8)
        return int.from_bytes(byte,byteorder='big',signed=True)
    elif what=="float":
        byte=fid.read(4)
        return unpack('>f',byte)[0]
    elif what=="double":
        byte=fid.read(8)
        return unpack('>d',byte)
    elif what=="character":
        byte=fid.read(n)
        return byte.decode('utf-8')
    else:
        return

