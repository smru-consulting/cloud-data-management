from PamBuoyDataManipulation.BinaryUnpack.pamBinRead import pamBinRead

def readNoiseMonData(fid,fileInfo,data,debug):
    dataLength = pamBinRead(fid,'int32',n=1)
    if dataLength==0:
        return data
    version = fileInfo['moduleHeader']['version']
    data['iChan'] = pamBinRead(fid,'int16',n=1)
    data['nBands'] = pamBinRead(fid,'int16',n=1)
    if(version>=1):
        data['nMeasures'] = pamBinRead(fid,'int16',n=1)
    else:
        data['nMeasures'] = 4

    n = [[0 for x in range(data['nBands'])] for y in range(data['nMeasures'])]
    m = [ [0] *2] *3
    if(version<=1):
        for i in range(data['nBands']):
            for j in range(data['nMeasures']):
                n[i][j] = pamBinRead(fid,'float',n=1)
    else:
        for j in range(data['nBands']):
            for i in range(data['nMeasures']):
            
                n[i][j] = pamBinRead(fid,'int16',n=1)/100
    
    data['bands'] = fileInfo['moduleHeader']['bands']
    data['rms'] = n[0]
    data['peak'] = n[1]
    del data['nMeasures']
    del data['nBands']

    return data