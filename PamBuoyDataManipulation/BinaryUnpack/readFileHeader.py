from PamBuoyDataManipulation.BinaryUnpack.pamBinRead import pamBinRead
from PamBuoyDataManipulation.BinaryUnpack.readJavaUTFString import readJavaUTFString

#This should be the first object parsed in the binary file. 
#Confirmed to unpack Decimus binary header data correctly. 
def readFileHeader(file):
    header ={}    
    header["length"] = pamBinRead(file,what='int32',n=4)
    header["identifier"] = pamBinRead(file,what='int32',n=4)
    header["fileFormat"] = pamBinRead(file,what='int32',n=4)
    header["pamguard"] = pamBinRead(file,what='character',n=12)
    header["version"] = readJavaUTFString(file)
    if "2." in header["version"]:
        header["version"] = 2
    header["branch"] = readJavaUTFString(file)
    header["dataDate"] = pamBinRead(file,what='int64',n=1)/1000
    header["analysisDate"] = pamBinRead(file,what='int64',n=1)/1000
    header["startSample"] = pamBinRead(file,what='int64',n=1)
    header["moduleType"] = readJavaUTFString(file)
    header["moduleName"] = readJavaUTFString(file)
    header["streamName"] = readJavaUTFString(file)
    header["extraInfoLen"] = pamBinRead(file,'int32',n=1)

    file.seek(header["extraInfoLen"],1)
    return header