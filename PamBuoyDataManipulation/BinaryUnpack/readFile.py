from PamBuoyDataManipulation.BinaryUnpack.pamBinRead import pamBinRead
from PamBuoyDataManipulation.BinaryUnpack.readFileHeader import readFileHeader
from PamBuoyDataManipulation.BinaryUnpack.readWMData import readWMData
from PamBuoyDataManipulation.BinaryUnpack.readWMDataHeader import readWMDataHeader
from PamBuoyDataManipulation.BinaryUnpack.readStdModuleFooter import readStdModuleFooter
from PamBuoyDataManipulation.BinaryUnpack.readStdModuleHeader import readStdModuleHeader
from PamBuoyDataManipulation.BinaryUnpack.readPamData import readPamData
from PamBuoyDataManipulation.BinaryUnpack.readNoiseMonData import readNoiseMonData
from PamBuoyDataManipulation.BinaryUnpack.readNoiseMonHeader import readNoiseMonHeader
from PamBuoyDataManipulation.BinaryUnpack.readNoiseBandData import readNoiseBandData
from datetime import datetime

def readFile(filepath):
    with open(filepath, mode='rb') as file: # b is important -> binary
        #meta data to be used in the unpacking process. Currently not used for any data output
        fileInfo = {}
        #list of output data units
        data=[]
        fileInfo['fileName'] = filepath
        fileInfo['readModuleHeader'] = readStdModuleHeader
        fileInfo['readModuleFooter'] = readStdModuleFooter
        prevPos=-1

        while True:
            #if we are stuck on a byte, get out!
            pos = file.tell()
            if(pos==prevPos):
               break
            prevPos=pos

            #Read the length of the next object
            nextLen = pamBinRead(file,'int32',n=1)
            #Read the type of the next object. 
            nextType = pamBinRead(file,'int32',n=1)

            #If the length of the next object is 0, get out!
            if nextLen==0:
                break
            #Back up to the beginning of the object to include length and type as the object is processed. 
            file.seek(-8,1)
            #type=-1 means the next object is the file header
            if nextType==-1:
                fileInfo['fileHeader'] = readFileHeader(file)
                #Assign the correct module functions to the fileInfo object
                if fileInfo['fileHeader']['moduleType']=="WhistlesMoans":
                    fileInfo["objectType"] = 2000
                    fileInfo["readModuleHeader"] = readWMDataHeader
                    fileInfo["readModuleData"] = readWMData
                    fileInfo["readBackgroundData"] = -1
                elif fileInfo['fileHeader']['moduleType']=='Noise Band':
                    fileInfo["objectType"] = 1
                    fileInfo["readModuleHeader"] = readNoiseMonHeader
                    fileInfo["readModuleData"] = readNoiseMonData
                elif fileInfo['fileHeader']['moduleType']=='NoiseBand':
                    fileInfo["objectType"] = 1
                    fileInfo["readModuleData"] = readNoiseBandData
            #type=-2 means the next object is the file footer. Have not yet refactored this logic
            elif nextType==-2:
                x=1
                break
            elif nextType==-3:
                fileInfo["moduleHeader"] = fileInfo["readModuleHeader"](file)
            elif nextType==-4:
                fileInfo["moduleFooter"] = fileInfo["readModuleFooter"](file,fileInfo)
            #otherwise, we have PAM data. The correct function handle was set when the file header was opened
            else:
                pamData = readPamData(file,fileInfo)
                data.append(pamData)
                x=1
    file.close()
    return data

#nn = readFile("C:\\Deployments\\20220207_LINEKILN\\sampleBinary\\NoiseBand_Noise_Noise_20220208_190000.pgdf")
#dd = readFile("C:\\AzureDevelopment\\cloud-data-management\\AzureStaticData\\deviceData\\pb325\\binDataPermanent\\20220128\\WhistlesMoans_Moans_Contours_20220128_041100.pgdf")
#print(dd)