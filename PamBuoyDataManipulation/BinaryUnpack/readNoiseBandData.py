from PamBuoyDataManipulation.BinaryUnpack.pamBinRead import pamBinRead

def readNoiseBandData(fid,fileInfo,data,debug):
    dataLength = pamBinRead(fid,'int32',n=1)
    if dataLength==0:
        return data
    version = fileInfo['moduleHeader']['version']

    if version<=2:
        data['startSample']=pamBinRead(fid,'int64',n=1)
        data['channelMap'] = pamBinRead(fid,'int32',n=1)

    data['rms'] = pamBinRead(fid,'int16',n=1)/100
    data['zeroPeak'] = pamBinRead(fid,'int16',n=1)/100
    data['peakPeak'] = pamBinRead(fid,'int16',n=1)/100

    if version>=2:
        data['sel'] = pamBinRead(fid,'int16',n=1)/100
        data['selSecs'] = pamBinRead(fid,'int16',n=1)

    return data