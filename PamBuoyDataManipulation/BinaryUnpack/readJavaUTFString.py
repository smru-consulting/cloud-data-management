from PamBuoyDataManipulation.BinaryUnpack.pamBinRead import pamBinRead

#Special character readings from JAVA UTF. First 2 bits define length of the string, and then the string is parsed. 
def readJavaUTFString(fid):
    length = pamBinRead(fid,'int16',n=1)
    str = pamBinRead(fid,'character',n=length)
    return str