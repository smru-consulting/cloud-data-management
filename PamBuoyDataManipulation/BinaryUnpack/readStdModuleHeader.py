from PamBuoyDataManipulation.BinaryUnpack.pamBinRead import pamBinRead

def readStdModuleHeader(file):
    header = {}
    header["length"]=pamBinRead(file,'int32',n=1)
    header["identifier"]=pamBinRead(file,'int32',n=1)
    header["version"]=pamBinRead(file,'int32',n=1)
    header["binaryLength"]=pamBinRead(file,'int32',n=1)
    return header