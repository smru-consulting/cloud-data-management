from PamBuoyDataManipulation.BinaryUnpack.pamBinRead import pamBinRead
from PamBuoyDataManipulation.BinaryUnpack.readStdModuleHeader import readStdModuleHeader

def readWMDataHeader(file):
    header = readStdModuleHeader(file)
    #There seems to be no delay scale for decimus WhistleMoan binaries. Skiping this for now, maybe figure out later
    header["delayScale"] = pamBinRead(file,'int32',n=1)
    if 1==0:#(header["version"]>=1):
        header["delayScale"] = pamBinRead(file,'int32',n=1)
    return header