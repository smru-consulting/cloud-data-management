#Module designed to gather file-stored status variables and compile into MongoDB document
#Status are collected on the Decimus and sent to zDataStore. They are immediately parsed into perl hash in /home/decimus1/zDataStore/pb318/repo/statusCombined
#   as well as individual status variables in /home/decimus1/zDataStore/pb318/statusDir
#All reporting done in the status files are reflective either the instant of report, or in the case of detectors since the last report was made.


from datetime import datetime
from bson.objectid import ObjectId

def unpackStatus(deployment,statusDir):

    #Initialize status report object
    statusObj = {}

    #assign deployment reference
    statusObj["deployments_id"] = ObjectId(deployment["_id"])

    #Get the time of status update
    #with open(statusDir+"TIME",'r') as f:
    #    timestr = f.read()
    #    timestr = timestr.split('TIME ')[1]
    #    statustime  = datetime.strptime(timestr,'%Y/%m/%d %H:%M:%S')
    #    statusObj["UTC"] = statustime
    #    f.close()
    
    #Get the time of the last status update... Seems to be the same as TIME
    with open(statusDir+"LASTSTATUSTIME") as f:
        dtString = f.read()
        lastDt = datetime.strptime(dtString,"%Y%m%d%H%M%S")
        statusObj["UTC"] = lastDt
        daystr = lastDt.strftime("%Y%m%d")
        f.close()
    
    #Get voltage
    with open(statusDir+"BATTVOLTAGE",'r') as f:
        vstring = f.read()
        voltage = float(vstring.split("V")[0])
        statusObj["Voltage"] = voltage
        f.close() 

    #Get system state
    with open(statusDir+"CURSTATE") as f:
        currentDecimusState = f.read()
        if currentDecimusState=="ACQUIRE":
            statusObj["SystemActive"] = True
        else:
            statusObj["SystemActive"] = False
        f.close()

    #Get retry elements
    with open(statusDir+"RETRYELEMENTS") as f:
        statusObj["RetryElements"] = int(f.read())
        f.close()

    #Get reboot count for the day
    with open(statusDir+"RESTARTCOUNT-"+daystr) as f:
        statusObj["DailyRebootCount"] = int(f.read())
        f.close()

    #Get uptime
    with open(statusDir+"LASTSTARTUPTIME") as f:
        laststartuptime = datetime.strptime(f.read(),'%Y/%m/%d %H:%M:%S')
        now = datetime.utcnow()
        uptime = now-laststartuptime
        statusObj["Uptime"] = uptime.days + float(uptime.seconds)/86400
        f.close()
    
    return statusObj