from os import stat
import sys

from datetime import datetime
sys.path.insert(1,'../../')
from DataInterface.Deployments import getActiveDeployments
from DataInterface.Detections import assignDetectionsRecording
from DataInterface.DetectionAlerts import assignDetectionAlertsRecording
import DataInterface.Status as Status
from PamBuoyDataManipulation.DecimusDatabasePopulation.processRecordings import processRecordings
from PamBuoyDataManipulation.DecimusDatabasePopulation.processBinary import processBinary
from PamBuoyDataManipulation.DecimusDatabasePopulation.processStatus import processStatus
from SystemMonitor.sendMail import alertEngineer
from SystemMonitor.sendMail import alertDetection
from SystemMonitor.WMMonitor import initializeDetectionAlert
from SystemMonitor.CheckStatus import CheckSystemStatus
from SystemMonitor.logResponse import log

#First grab all the active CABOW deployments from the database
activeDeployments = getActiveDeployments("CABOW")

#For each active deployment, process binary and process the status
for deployment in activeDeployments:
    
    #Process PAMGaurd binary data for the deployment
    try:
        newDetections = processBinary(deployment)
        if len(newDetections)>0:
            initializeDetectionAlert(deployment,newDetections)

    #If there is an error, alert engineer of it
    except:
        alertEngineer("Error in processing binary data","",deployment['instrument']["ID"])
        print("ERROR BINARY")
    #Process the PAMBuoy Status data for the deployment
    try:
        status = processStatus(deployment)
        status = Status.getStatusbyID(status.inserted_id)
        systemUp = CheckSystemStatus(deployment,status)
        if systemUp:
            log(datetime.utcnow(),"System up for "+str(status["Uptime"]),deployment['instrument']['ID'])
        else:
            log(datetime.utcnow(),"Something failed in status check",deployment['instrument']['ID'])
    #If there is an error, alert the engineer of it
    except:
        alertEngineer("Error in processing status data",'',deployment['instrument']["ID"])
        print("ERROR STATUS")
    try:
        newRecordings = processRecordings(deployment)
        if len(newRecordings)>0:
            for recording in newRecordings:
                assignDetectionsRecording(recording)
                log(datetime.utcnow(),"New recording received. Alert should send ",deployment['instrument']['ID'])
                alert = assignDetectionAlertsRecording(recording)
                if not alert==None:
                    alertDetection(deployment,alert,recording)
    except:
        alertEngineer("Error in processing recordings data",'',deployment['instrument']["ID"])
        print("ERROR RECORDINGS")

