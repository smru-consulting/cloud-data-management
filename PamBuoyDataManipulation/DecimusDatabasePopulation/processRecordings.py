import sys
import os
import shutil
from datetime import datetime
from bson.objectid import ObjectId

import PamBuoyDataManipulation.RecordingUnpack.RecordingConverter as RecordingConverter
import DataInterface.Recordings as Recordings


def processRecordings(deployment):
    #At this point the only deployments that are input to this should be CABOW, but double check
    if deployment['instrument']['type']!='CABOW':
        return

    #Get temporary binary data path
    tmpRecordingStore = getTmpRecordingStore(deployment)
    #get permanent binary store path for each file type
    permStores = {}
    permFiles = {}
    permStores["x3a"] = getPermRecordingsStore(deployment,'x3a')
    permStores["wav"] = getPermRecordingsStore(deployment,'wav')
    permStores["mp3"] = getPermRecordingsStore(deployment,'mp3')

    #get the start date of the deployment and convert to integer to conpare with all other date folders in directory
    deploymentStartDate = deployment["deploymentDate"]
    deploymentStartDateInt = int(deploymentStartDate.strftime("%Y%m%d"))

    #Initialize new list of recordings
    newRecordings = []

    #For each date folder in the instrument's binary data directory, find the .pgdf files we are interested in.
    for datefolder in os.listdir(tmpRecordingStore):
        if "smru" in datefolder:
            continue
        datefoldint = int(datefolder)
        #If the date folder is older than the start of the deployment, ignore it
        if(datefoldint<deploymentStartDateInt):
            continue
        #Make sure the datefolder exists in each permanent store location
        for fileType,permDir in permStores.items():
            if not os.path.exists(os.path.join(permDir,datefolder)):
                os.makedirs(os.path.join(permDir,datefolder))
        #Otherwise the data is of interest and we should unpack it!
        for x3FileName in os.listdir(os.path.join(tmpRecordingStore,datefolder)):
            tmpLocation = os.path.join(tmpRecordingStore,datefolder,x3FileName)
            for filetype in permStores:
                permFiles[filetype] = os.path.join(permStores[filetype],datefolder,x3FileName.replace("x3a",filetype))
            permFiles['xml'] = os.path.join(permStores['wav'],datefolder,x3FileName.replace("x3a",'xml'))
            RecordingConverter.x3toWAV(tmpLocation,permFiles['wav'])
            recordingMetaData = RecordingConverter.getRecordingMeta(permFiles)
            recordingMetaData['mp3']['fs'] = RecordingConverter.WAVtoMP3(permFiles['wav'],permFiles['mp3'])
            shutil.move(tmpLocation,permFiles['x3a'])
            newRecording = generateRecordingDocument(recordingMetaData,permFiles,deployment)
            if newRecording == None:
                continue
            else:
                newRecordings.append(newRecording)
    return newRecordings
                

def generateRecordingDocument(recordingMetaData,permFiles,deployment):
    newRecording = {}
    filename_split = os.path.basename(permFiles['x3a']).split('.')[0].split('_')
    UTC = datetime.strptime(filename_split[1]+filename_split[2],"%Y%m%d%H%M%S")
    newRecording['UTC'] = UTC
    newRecording.update(recordingMetaData)
    newRecording["wav"]['serverPath'] = permFiles['wav']
    newRecording['x3a']['serverPath'] = permFiles['x3a']
    newRecording['mp3']['serverPath'] = permFiles['mp3']
    newRecording['deployments_id'] = ObjectId(deployment['_id'])
    return Recordings.newRecording(newRecording)


#Returns the temporary recordings storage location for a Decimus deployment.
#This is where the recordings are initially unpacked. 
def getTmpRecordingStore(deployment):
    buoyID = deployment['instrument']['ID']
    buoynum = buoyID.split("pb")[1]
    #Testing directory
    return "C:\\AzureDevelopment\\cloud-data-management\\AzureStaticData\\deviceData\\"+buoyID+"\\smruDa-pb-"+buoynum+"-01-recs"
    #return "/home/decimus1/deviceData/"+buoyID+"/smruDa-pb-"+buoynum+"-01-recs/"

#Returns the permanent directory for 
def getPermRecordingsStore(deployment,filetype):
    buoyID = deployment['instrument']['ID']
    #testing directory
    return "C:\\AzureDevelopment\\cloud-data-management\\AzureStaticData\\deviceData\\"+buoyID+"\\recsPermanent\\"+filetype
    #return "/home/decimus1/deviceData/"+buoyID+"/recsPermanent/"+filetype+"/"