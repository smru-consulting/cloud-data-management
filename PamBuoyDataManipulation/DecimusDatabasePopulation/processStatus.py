import sys

from PamBuoyDataManipulation.StatusUnpack.UnpackStatus import unpackStatus
from DataInterface.Status import commitStatus


#processStatus
#Input deployment object directly from mongodb
#Process: for a given deployment, locate the statusDir, call UnpackStatus to return a status document for the deployment, and commit the document to the database
def processStatus(deployment):
    #At this point the only deployments that are input to this should be CABOW, but double check
    if deployment['instrument']['type']!='CABOW':
        return
    
    #Get the pbID, which corresponds to the statusDir in the server
    pbID = deployment['instrument']["ID"]
    #Get the statusDir in the server
    #Testing directory
    statusDir = "C:\\AzureDevelopment\\cloud-data-management\\AzureStaticData\\zDataStore\\"+pbID+"\\statusDir\\"
    #statusDir = "/home/decimus1/zDataStore/"+pbID+"/statusDir/"
    

    #Get the status report document from UnpackStatus
    statusReport = unpackStatus(deployment,statusDir)

    #Commit status report to the database
    return commitStatus(statusReport)