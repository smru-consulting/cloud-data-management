#Process binary function to use from a data control module 
#input to processbinary is a deployment object from mongodb
#The process uses the instrument ID to search for relevant binary files in the Azure server. 

import os
import sys
from datetime import datetime
import shutil

from bson.objectid import ObjectId

from PamBuoyDataManipulation.BinaryUnpack.readFile import readFile
from DataInterface.Detections import newDetection


#processBinary
#Input deployment object directly from mongodb
#Process: check server binary paths for unprocessed binary files after deployment date, unpack the file using the BinaryUnpack.readFile module,
#  and commit to database in appropriate collection
def processBinary(deployment):
    #At this point the only deployments that are input to this should be CABOW, but double check
    if deployment['instrument']['type']!='CABOW':
        return
    
    #Get temporary binary data path
    tmpBinaryStore = getTmpBinaryStore(deployment)
    #get permanent binary store path
    permBinaryStore = getPermBinaryStore(deployment)

    #get the start date of the deployment and convert to integer to conpare with all other date folders in directory
    deploymentStartDate = deployment["deploymentDate"]
    deploymentStartDateInt = int(deploymentStartDate.strftime("%Y%m%d"))

    newDetections = []

    #For each date folder in the instrument's binary data directory, find the .pgdf files we are interested in.
    for datefolder in os.listdir(tmpBinaryStore):
        if "smru" in datefolder:
            continue
        datefoldint = int(datefolder)
        #If the date folder is older than the start of the deployment, ignore it
        if(datefoldint<deploymentStartDateInt):
            continue
        if not os.path.isdir(os.path.join(permBinaryStore,datefolder)):
            os.mkdir(os.path.join(permBinaryStore,datefolder))
        for binaryFile in os.listdir(os.path.join(tmpBinaryStore,datefolder)):
            #If it is detection data, read it
            if 'Moans' in binaryFile:
                PAMDataArray = readFile(os.path.join(tmpBinaryStore,datefolder,binaryFile))
                #For each data unit in the returned array of data creaate a new detection and commit it.
                for dataUnit in PAMDataArray:
                    detection = newDetectionDocument(deployment,dataUnit)
                    if detection == None:
                        continue
                    else:
                        newDetections.append(detection)
            elif 'Noise' in binaryFile:
                xx=1
            #Regardless of file type, move it to the permanent binData directory, in the same datefolder name. 
            shutil.move(os.path.join(tmpBinaryStore,datefolder,binaryFile),os.path.join(permBinaryStore,datefolder,binaryFile))
    return newDetections
                        

def newDetectionDocument(deployment,detection):
    detection["deployments_id"] = ObjectId(deployment["_id"])
    detection["UTC"] = datetime.utcfromtimestamp(detection["millis"]/1000.0)
    del detection["date"]
    del detection["identifier"]
    del detection["flagBitMap"]
    #commit to the database
    return newDetection(detection)
    
#Returns the temporary binary storage location for a Decimus deployment.
#This is where the binary data are initially unpacked. 
def getTmpBinaryStore(deployment):
    buoyID = deployment['instrument']['ID']
    buoynum = buoyID.split("pb")[1]
    #Testing directory
    return "C:\\AzureDevelopment\\cloud-data-management\\AzureStaticData\\deviceData\\"+buoyID+"\\smruDa-pb-"+buoynum+"-01-binData"
    #return "/home/decimus1/deviceData/"+buoyID+"/smruDa-pb-"+buoynum+"-01-binData/"

#Returns the permanent 
def getPermBinaryStore(deployment):
    buoyID = deployment['instrument']['ID']
    #testing directory
    return "C:\\AzureDevelopment\\cloud-data-management\\AzureStaticData\\deviceData\\"+buoyID+"\\binDataPermanent"
    #return "/home/decimus1/deviceData/"+buoyID+"/binDataPermanent/"