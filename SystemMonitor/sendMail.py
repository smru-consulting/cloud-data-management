from re import sub
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from datetime import timedelta

import DataInterface.DetectionAlerts as DetectionAlerts


#All Send Mail functionality accessed in System
#Engineering notifications sent if there is error in the processing of data or if the remote unit has malfunctioned
#Detections notifications sent to subscribers of the buoy

def alertEngineer(alertMessage,error,instID):
    subject = "Alert for "+instID
    html = "<div><strong>"+alertMessage+"</strong><p>"+"</p></div>"
    sendMessage(["st@smruconsulting.com"],subject,html)

def alertDetection(deployment,alert,recording):
    instrumentID = deployment['instrument']["ID"]
    startTimeStr = (alert['startTime']-timedelta(hours=8)).strftime("%m/%d/%Y %H:%M")
    endTimeStr = (alert['endTime']-timedelta(hours=8)).strftime("%m/%d/%Y %H:%M")
    location = deployment["locationName"]
    lat=deployment['lat']
    lon=deployment['lon']
    template_id = 'd-b2f331d1399745d68f796035d850c934'
    dataObj = {'instrumentID':instrumentID,
                'startDateStr':startTimeStr,
                'endDateStr':endTimeStr,
                'shortName':location,#["shortName"],
                'extension':location,#["extension"],
                'detectionCount':alert['detectionCount'],
                'lat':lat,
                'lon':lon,
                'uri':recording['mp3']['serverPath']}
    sendTemplateMessage(deployment["subscribers"],template_id,dataObj)
    DetectionAlerts.sentAlertMessage(alert)


#Function: simple email handler using the sendgrid API
#Params: reievers: array of email addresses to send email
#        subject: String subject line for email
#        HTML: HTML chunk to populate in email and send
def sendTemplateMessage(recievers,template_id,dynamicObject):
    message = Mail(
        from_email='cab@smruconsulting.com',
        to_emails=recievers)
    message.dynamic_template_data = dynamicObject
    message.template_id = template_id
    try:
        sg = SendGridAPIClient('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')
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e.message)

def sendMessage(recievers,subject,html):
    message = Mail(
        from_email='cab@smruconsulting.com',
        to_emails=recievers,
        subject=subject,
        html_content=html)
    
    try:
        sg = SendGridAPIClient('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')
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e.message)