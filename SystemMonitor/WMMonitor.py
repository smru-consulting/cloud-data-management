
from bson.objectid import ObjectId
from datetime import datetime
from datetime import timedelta
import calendar
import xml.etree.ElementTree as ET

import DataInterface.DetectionAlerts as DetectionAlerts
import SystemMonitor.sendMail as sendMail

#Called to create a new detection alert. Generates a recording request for the densest minute of detections in a set of new detections
#Input: Deployment object reflecting data structure for deployments 
#       newDetections: list of detection objects relecting data structure for detections 
def initializeDetectionAlert(deployment,newDetections):
    densestMinute = queueDensistMinute(deployment,newDetections)
    generateDetectionAlertDocument(deployment,newDetections,densestMinute)
    sendMail.alertEngineer("New detection alert event on buoy. Recording requested. Recording Request: "+densestMinute.strftime("%Y\%m\%d %H:%M"),"",deployment['instrument']["ID"])

#Called to generate a new DetectionAlert object reflecting data structure and send to the data interface for handling.
#Input: deployment: object reflecting data structure
#       newDetections: list of detection objects reflecting data structure
#       densestMinute: python datetime object of the densest minute of detections in a list of new detection objects
def generateDetectionAlertDocument(deployment,newDetections,densestMinute):
    detectionAlert = {'notificationDelivered':False,'recordingRecieved':False,'deployments_id':ObjectId(deployment['_id'])}
    detectionTimes = []
    for detection in newDetections:
        detectionTimes.append(calendar.timegm(detection["UTC"].timetuple()))
    detectionAlert["startTime"]=datetime.utcfromtimestamp(min(detectionTimes))
    detectionAlert["endTime"]=datetime.utcfromtimestamp(max(detectionTimes))
    detectionAlert['detectionCount'] = len(newDetections)
    detectionAlert['recordingTimeRequested'] = densestMinute
    DetectionAlerts.newDetectionAlert(detectionAlert)

#Function called to queue the recording from the densist minute of detections in a date range
#Input: dateRange, array of length 2 with python datetime objects, marking the (inclusive) boundary of a date range to search
#Input: deployment, mongodb deployment object
def queueDensistMinute(deployment,newDetections):
    #initiate empty array of detection times
    detectionTimes = []
    #populate the array with UNIX timestamp for each new detection
    for detection in newDetections:
        detectionTimes.append(calendar.timegm(detection["UTC"].timetuple()))
    #define the max and min for the detection times
    dateRange = [min(detectionTimes),max(detectionTimes)]
    #define the duration of time for new detections
    tDur = dateRange[1]-dateRange[0]
    #make number of bins the number of minutes in the time frame
    nbins = int(tDur/60)+1
    #generate a list of time bins at resolution of 1 minute
    timeBins = [dateRange[0]+(i*tDur)/nbins for i in range(nbins)]
    #define array of 0s of length of the number of time bins
    counts = [0 for i in range(nbins)]
    #bin out the detection times into the counts array
    for dtime in detectionTimes:
        for tbini in range(nbins-1):
            if dtime>=timeBins[tbini] and dtime<=timeBins[tbini+1]:
                counts[tbini]+=1
    maxN,maxIdx = 0,0
    #grab the max number and the max index for the detection counts in the time range
    for bin in range(nbins):
        if counts[bin]>maxN:
            maxIdx = bin
    #get the left edge of the maximum time bin. max index is always 1 value
    maxMinutesLeftEdge = datetime.utcfromtimestamp(timeBins[maxIdx])
    #submit a recording request for the densest minute
    submitRecordingRequest(deployment,maxMinutesLeftEdge)
    return maxMinutesLeftEdge
    
#Function to submit a recording request for a specific buoy
#Params: deployment: the deployment data for a buoy. 
#        detectionTime: python datetime object of the time of a detection to grab. 
#WARNING: This function modifies an XML file which is queried by the remote unit. If the request for recordings is overwhelming, the remote unit will become unstable
def submitRecordingRequest(deployment,detectionTime):
    #Get the string for the date of the recording
    dateStr = detectionTime.strftime("%Y%m%d")
    #Set the last minute of recordings requested to the densest minute of recordings
    endTime = detectionTime
    #Set the first minute of recordings requested to the minute immediately prior to the densest minute
    startTime = detectionTime-timedelta(minutes=1)
    #Turn the start and end time to string format for submission to the request document
    startTimeStr = startTime.strftime("%H%M")
    endTimeStr = endTime.strftime("%H%M")
    #Get buoy ID as PK to the path for the recording request document
    buoyId = deployment["instrument"]["ID"]
    #Testing path
    buoyRestQueueLoc = "C:/AzureDevelopment/cloud-data-management/AzureStaticData/deviceData/"+buoyId+"/recordingsRequest/queue.xml"
    #buoyRestQueueLoc = "/home/decimus1/deviceData/"+buoyId+"/recordingsRequest/queue.xml"
    try:
        #Parse the recording request document
        tree = ET.parse(buoyRestQueueLoc)
        root = tree.getroot()
        for request in root.iter('request'):
            #if the minutes requested have already been requested, return and exit process
            if (request.attrib['dateStr']==dateStr and request.attrib['startTime']==startTimeStr and request.attrib['endTime']==endTimeStr):
                return
        #Grab the requests. "Requests" is the only child in root. 
        for child in root:
            #initialize new request
            newRequest = ET.SubElement(child,'request')
            #set dateStr to the appropriate date string, and set the start and end times. 
            newRequest.set('dateStr',dateStr)
            newRequest.set('endTime',endTimeStr)
            newRequest.set('staged','0')
            newRequest.set('startTime',startTimeStr)
        print(root)
        #Write the xml to the appropriate location. 
        with open(buoyRestQueueLoc, 'wb') as f:
            tree.write(f, encoding='utf-8')
    except:
        sendMail.alertEngineer("Error in requesting recording","",deployment['instrument']["ID"])