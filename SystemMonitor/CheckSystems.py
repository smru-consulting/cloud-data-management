#Driver of the system check program
#Execute to run system checks on all active deployments

import sys
from datetime import datetime
from datetime import timedelta

import SystemMonitor.CheckStatus as CheckStatus
import SystemMonitor.WMMonitor as WMMonitor
import SystemMonitor.sendMail as sendMail
import DataInterface.Deployments as Deployments
import DataInterface.Status as Status
#import DataInterface.DetectionAlerts as DetectionAlerts


#Function to run regular checks on all active CABOW deployments in the database. 
def CheckSystems():
    activeDeployments = Deployments.getActiveDeployments("CABOW")
    for deployment in activeDeployments:
        try:
            status = Status.getStatus(deployment)
        except:
            sendMail.alertEngineer("Error in retrieving status data",sys.exc_info()[0],deployment['instrument']["ID"])
            return
        isSystemFunctioning = CheckStatus.CheckSystemStatus(status,deployment)
        if isSystemFunctioning:
            #If there is a whistle/moan detector in the system, check for detections
            if "Moans" in deployment["dataModules"]:
                newDetections = WMMonitor.newDetectionCount(status)
                #If there are new detections, alert the subscribers and queue a recording from the densest minute of detections. 
                if newDetections>0:
                    #Get the last status time and set the time range for the detection period
                    lastStatus = Status.getLastStatus(deployment)
                    timeRange = [lastStatus["UTC"],status["UTC"]]
                    sendMail.alertDetection(deployment,0,timeRange,newDetections)
                    try:
                        #densestMinute = WMMonitor.queueDensistMinute(deployment,timeRange)
                        #sendMail.alertDetection(deployment,densestMinute,timeRange,newDetections)
                        x=1
                    except:
                        sendMail.alertEngineer("Detection event. Error in queuing recording",sys.exc_info()[0],deployment['instrument']["ID"])
                    
                    

#Call system check function
CheckSystems()