#All Send Mail functionality accessed in System
#Engineering notifications sent if there is error in the processing of data or if the remote unit has malfunctioned
#Detections notifications sent to subscribers of the buoy'

import os

pwd = os.getcwd()

def log(datetime,message,buoyID):
    logfile = os.path.join(pwd,'systemMonitorLogBeta.txt')
    #logfile = os.path.join(pwd,'SystemMonitor/systemMonitorLogBeta.txt')
    #logfile = os.path.join(pwd,'systemMonitorLog.txt')
    logstring = datetime.strftime("%m/%d/%Y %H:%M")+": "+buoyID+": "+message+'\n'
    with open(logfile,'a+') as f:
        f.write(logstring)
    f.close()