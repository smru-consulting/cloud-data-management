
from datetime import datetime
from datetime import timedelta

from SystemMonitor.sendMail import alertEngineer

#Hard coded 1 hour intervals for status updates. 
def isDataTransfer(status):
    timeSinceLast = datetime.utcnow()-status["UTC"]
    if timedelta(hours=1,minutes=10)<timeSinceLast:
        return False
    else:
        return True

#Check for basic status reports
def CheckSystemStatus(deployment,status):
    if not isDataTransfer(status):
        alertEngineer("Data transfer failure","Check the buoy",deployment['instrument']["ID"])
        return False
    if status["SystemActive"] == False:
        alertEngineer("System is not collecting data","Check the buoy",deployment['instrument']["ID"])
        return False
    if status["RetryElements"] > 1:
        alertEngineer("Retry elements increasing","Check the buoy",deployment['instrument']["ID"])
        return False
    if status["Voltage"] == 0:
        alertEngineer("Voltage reading 0","Check the buoy",deployment['instrument']["ID"])
        return False
    #If none of the above apply, assume system is okay
    return True