
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import timedelta

client = MongoClient(port=27017)
db = client["SMRUDataStore"]

#Functioned called to get all detections for a deployment in a date range
#Input: dateRange, array of length 2 with python datetime objects, marking the (inclusive) boundary of a date range to search
#Input: deployment, mongodb deployment object
#Output: array of python dicts for detection objects
def getDetections(deployment,dateRange):
    detections = db["Detections"]
    #execute query for all detections for a given deployment in date range
    detectionsCursor = detections.find({'deployments_id':ObjectId(deployment['_id']),'UTC':{"$gt":dateRange[0],"$lt":dateRange[1]}})
    detections=[]
    for i in range(detectionsCursor.count()):
        detection = detectionsCursor.next()
        detections.append(detection)
    return detections

#Commit a detection to the detections collection
def newDetection(detection):
    Detections = db["Detections"]
    result = Detections.insert_one(detection)
    if result.acknowledged:
        return Detections.find_one({'_id':ObjectId(result.inserted_id)})
    else:
        return

#assign the appropriate detections the FK of a recording received
def assignDetectionsRecording(recording):
    print("running function")
    detections = db["Detections"]
    startTime = recording['UTC']
    endTime = startTime+timedelta(seconds=recording['duration_seconds'])
    query = {'deployments_id':ObjectId(recording['deployments_id']),'UTC':{"$gt":startTime,"$lt":endTime}}
    newvalues = { "$set": { "recordings_id":ObjectId(recording['_id']) } }
    detections.update_many(query,newvalues)