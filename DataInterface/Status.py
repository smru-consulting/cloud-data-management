from pymongo import MongoClient
from bson.objectid import ObjectId

#Interface to status collection in database

client = MongoClient(port=27017)
db = client["SMRUDataStore"]

#New status report commit to the database
def commitStatus(statusReport):
    Status = db["Status"]
    return Status.insert_one(statusReport)

#Get a status document by ID reference
def getStatusbyID(id):
    Status = db["Status"]
    return Status.find_one({'_id':ObjectId(id)})

#Get the most recent status update from a deployment
def getStatus(deployment):
    status = db["Status"]
    #get the most recent status document from the status collection
    statusCursor = status.find({'deployments_id':ObjectId(deployment['_id'])}).sort("UTC",-1)
    currentStatus = statusCursor.next()
    return currentStatus

#Get the second to most recent status update for a deployment. 
def getLastStatus(deployment):
    status = db["Status"]
    #get the most recent status document from the status collection
    statusCursor = status.find({'deployments_id':ObjectId(deployment['_id'])}).sort("UTC",-1)
    currentStatus = statusCursor.next()
    lastStatus = statusCursor.next()
    return lastStatus