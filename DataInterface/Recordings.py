from pymongo import MongoClient
from bson.objectid import ObjectId

client = MongoClient(port=27017)
db = client["SMRUDataStore"]


def getRecordings(deployment,dateRange):
    Recordings = db["Recordings"]
    #execute query for all detections for a given deployment in date range
    detectionsCursor = Recordings.find({'deployments_id':ObjectId(deployment['_id']),'UTC':{"$gt":dateRange[0],"$lt":dateRange[1]}})
    detections=[]
    for i in range(detectionsCursor.count()):
        detection = detectionsCursor.next()
        detections.append(detection)
    return detections

def getRecording(alert):
    Recordings = db["Recordings"]
    return Recordings.find_one({'_id':ObjectId(alert['recordings_id'])})

#Commit a detection to the detections collection
def newRecording(recording):
    Recordings = db["Recordings"]
    result = Recordings.insert_one(recording)
    if result.acknowledged:
        return Recordings.find_one({'_id':ObjectId(result.inserted_id)})
    else:
        return
