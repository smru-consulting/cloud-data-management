#Data interface to Detection Alerts collection in the database

from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime, timedelta


client = MongoClient(port=27017)
db = client["SMRUDataStore"]

#New detection alert consisting of deployment reference, recording request queue time, time range for detections, and new detection count
def newDetectionAlert(newDetectionAlert):
    detectionAlerts = db["DetectionAlerts"]
    detectionAlerts.insert_one(newDetectionAlert)

#Get all detection alerts which have not sent a notification
#Returns list of unprocessed detection alerts. 
def getUnprocessedDetectionAlerts(deployment):
    detectionAlerts = db["DetectionAlerts"]
    return detectionAlerts.find({'deployments_id':ObjectId(deployment['_id']),'notificationDelivered':False})

#assign the appropriate detections the FK of a recording received
def assignDetectionAlertsRecording(recording):
    detectionAlerts = db["DetectionAlerts"]
    recordingTime = [recording['UTC'],recording['UTC']+timedelta(seconds=recording['duration_seconds'])]
    query = {'recordingRecieved':False}
    alerts = detectionAlerts.find(query)
    pertinantAlert = None
    for detectionAlert in alerts:
        alertTimeFrame = [detectionAlert['startTime'],detectionAlert['endTime']]
        earliestEndTime = min([alertTimeFrame[1],recordingTime[1]])
        lastestStartTime = max([alertTimeFrame[0],recordingTime[0]])
        if (earliestEndTime-lastestStartTime)>=timedelta(0):
            pertinantAlert = detectionAlert
    if not pertinantAlert==None:
        newvalues = { "$set": { "reference_recordings_id":ObjectId(recording['_id']) ,'recordingRecieved':True} }
        detectionAlerts.update_one({'_id':ObjectId(pertinantAlert['_id'])},newvalues)
    return pertinantAlert

def sentAlertMessage(alert):
    detectionAlerts = db["DetectionAlerts"]
    detectionAlerts.update_one({'_id':ObjectId(alert['_id'])},{"$set":{'notificationDelivered':True}})
    return 