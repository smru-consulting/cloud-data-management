#Data interface for deployments collection in database

from pymongo import MongoClient
from bson.objectid import ObjectId

client = MongoClient(port=27017)
db = client["SMRUDataStore"]

#Get all active deployments in the deployments collection
def getActiveDeployments(instrumentType):
    Deployments = db["Deployments"]
    activeDeployments = Deployments.find({'active':True,'instrument.type':instrumentType})
    print(activeDeployments)
    return activeDeployments