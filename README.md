# SMRU Cloud Data Management Code #

This respository is dedicated to all initial programs relevant to the Cloud Data Management project. 

The intention of the project is to provide a normalized data platform for the SMRU Baseline deployments, monitor the stability of the deployments, and provide dynamic tools for viewing and extracting data. 

There are three primary independently ran programs relavent to the project:

* PamBuoy Data Manipulation Program
* System monitor and response program
* Data interface program

### PamBuoy Data Manipulation Program ###
This program is written in python and is intended to extract the data stored on the SAIL server structure and commit it to a database using the schema defined within the project.

#### To Process Data, execute python file PamBuoyDataManipulation.DecimusDatabasePopulation.processData.py ####

This consists of three primary functions for a Decimus-based remote unit:

* Unpack PAMGuard binary files to a python object
* Collect status updates from the status directory
* Commit all new data to the database

 
The PAMBuoy status and binary parsing will be specific to Decimus-based systems, but the focus of the program is to create a scalable and modular system to include other sources of data collection.

### System Monitor and Response Program ###
This program is intended to monitor the status updates of the remote systems, and take appropriate action when certain criteria are met. 

#### To run system check program, execute python file SystemMonitor.CheckSystems.py ####

This program too is focused on scalability and modularity to take action with new data modules introduced to the system.

### Data Interface Program ###
This program is designed to provide a centralized interface to the database. There are currently some leaks and a lack of well-defined protocol with committing and querying data. It is in progress to clean that up